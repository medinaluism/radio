import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NavController, MenuController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { FCM } from '@ionic-native/fcm/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  @Input() texto = 'Cargando...';
  @Input() sms = '¡Campos Incompletos!';
  @Input() sms_usr = '¡El usuario no existe!';
  @Input() sms_usr_ex = '¡El usuario ya existe!';
  @Input() sms_pas = '¡Contraseña Incorrecta!';
  @Input() sms_pass_ig = '¡Las contraseñas deben ser iguales!';
  bandalerta = false;
  bandalerta_us = false;
  bandalerta_us_ex = false;
  bandalerta_pas = false;
  band_pass_iguales = false;
  loader = false;
  type: string;
  typepassd: string;
  typepass: string;
  passworddos: any;
  usuarios;
  //login con usuario creado
  logeo = {
    email: '',
    password: ''
  }
  dato;
  windhome = false;
  vent0 = false;
  vent1 = false;
  vent2 = false;
  vent3 = false;
  vent4 = false;
  vent5 = false;
  vent6 = false;
  registerUser = {
    name: '',
    email: '',
    password: '',
    location: ''
  }
  send = {
    userAppId: 1
  }
  constructor(private route: Router, private http: HttpClient, private googlePlus: GooglePlus,
    private navCtrl: NavController, private menu: MenuController, private fcm: FCM) { }
    token_usuario_logeado;
    token_activo;
    valor;
    inf_notificacion;
  ngOnInit() {
    this.fcm.getToken().then(token => {
      this.valor = token;
      localStorage.setItem('token_act',this.valor);
      const token_devie = localStorage.getItem('token_act');
      console.log("Mi token Device: ",token_devie);
      alert("Mi token: "+token_devie);
      console.log("Aqui Obtengo mi token: ",this.valor);
    });
      this.windhome = true; 
      this.menu.enable(false);
  }
  
  createUser(){   //Usuario Nuevo
    this.responseCreateUser().subscribe(solicitud=>{
      if(solicitud.status === 400){
        this.loader= false;
        this.bandalerta_us_ex = true;
        setTimeout(() => {
          this.bandalerta_us_ex = false;
          this.sms_usr_ex = '¡El usuario ya existe!';
        }, 1000);
      }else if(solicitud.status === 201){
        //Sesion activa
        this.loader= false;
        this.dato = 5;
        localStorage.setItem('active',this.dato);
        console.log("D: ",this.dato);

        const dat_user = solicitud;
        this.user_loged = dat_user.data;
        localStorage.setItem('logeado',JSON.stringify(this.user_loged));
        console.log("Crear usuario y cerrar sesion: ",localStorage.getItem('logeado'));
        localStorage.setItem('ema_il', this.user_loged.email);
        localStorage.setItem('pass_word', this.user_loged.password);
        const access_log = solicitud.data;
        console.log("Usuario Creado: ",access_log);
        this.loader = false;
        this.route.navigate(['/home']);
        this.menu.enable(true);
        this.vent0 = false;
        this.vent1 = false;
      } 
    })
  }
  responseCreateUser(): Observable<any>{
    this.loader = true;
    const valor_token = localStorage.getItem('token_act');
    const request = {
      name: this.registerUser.name,
      email:  this.registerUser.email,
      password: this.registerUser.password,
      location: this.registerUser.location,
      //tokenDevice: valor_token
    }
    console.log("Mando: ",request);
    const dir = 'https://hyperiontechlab.com:9005/usuarioApp/insertUser';
    const json = JSON.stringify(request)
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    console.log("Mando: "+json);
    return this.http.post(dir, json, httpOptions);
  }
   
   Option(val){
     this.registerUser.location = val.detail.value;
   }
 
   register_inicio(){
     this.loader = true;
     setTimeout(() => {
       this.loader = false;
       this.vent0 = false;
       this.vent1 = true;
     }, 500);
   }
 
   access_user(){
     this.loader =false;
     if(this.registerUser.name === undefined || this.registerUser.name === '' || this.registerUser.name === null || this.registerUser.email === undefined || this.registerUser.email === '' || this.registerUser.email === null ||
     this.registerUser.password === undefined || this.registerUser.password === '' || this.registerUser.password === null || this.registerUser.location === undefined || this.registerUser.location === '' || this.registerUser.location === null){
       this.bandalerta = true;
       setTimeout(() => {
         this.bandalerta = false;
         this.sms = '¡Campos Incompletos!';
       }, 1000);
     }else{
       this.route.navigate(['/home']);
       /*if(this.registerUser.password === this.passworddos){
        this.createUser();
       }else{
         this.band_pass_iguales = true;
         setTimeout(() => {
           this.band_pass_iguales = false;
           this.sms_pass_ig = '¡Las contraseñas deben ser iguales!';
         }, 1000);
       }*/
     }
   }

   window_home(){
     this.windhome = false;
     this.vent0 = true;
   }

   login(){
     this.loader = true;
     setTimeout(() => {
       this.loader = false;
       this.vent0 = false;
       this.vent2 = true;
     }, 500);
   }
       
   acceso_usuario(){
     if(this.logeo.email === null || this.logeo.email === '' || this.logeo.password === null || this.logeo.password === ''){
       this.bandalerta = true;
       setTimeout(() => {
         this.sms = '¡Campos Incompletos!';
         this.bandalerta = false;
       }, 1000);
     }else{
       this.responseLogin().subscribe(solicitud=>{
         console.log("Respuesta: ",solicitud);
         if(solicitud.status === 200){
           this.loader = false;
           this.dato = 5;
          localStorage.setItem('active',this.dato);
          console.log("D: ",this.dato);
          const dat_user = solicitud;
          this.user_loged = dat_user.data;
          console.log("Logeado: ",this.user_loged);
          localStorage.setItem('logeado',JSON.stringify(this.user_loged));
          localStorage.setItem('ema_il', this.user_loged.email);
          localStorage.setItem('pass_word', this.user_loged.password);
          this.menu.enable(true);
          this.loader = false;
          this.route.navigate(['/home']);
          this.vent0 = false;
          this.vent1 = false;
          this.logeo.email = null;
          this.logeo.password = null;
         }else if(solicitud.status === 400){
           this.loader = false;
           this.bandalerta_pas = true;
           setTimeout(() => {
             this.sms_pas = '¡Contraseña Incorrecta!';
             this.bandalerta_pas = false;
           }, 1000);
         }else if(solicitud.status === 404){
           this.loader = false;
           this.bandalerta_us = true;
           setTimeout(() => {
             this.sms_usr = '¡El usuario no existe!';
             this.bandalerta_us = false;
           }, 1000);
         }
       }, err=>{
         if(err.status === 500){
           console.log("Error Inesperado");
         }
       })
     }
   }
   
   user_loged;
   responseLogin(): Observable<any>{
     this.loader = true;
     const dir = 'https://hyperiontechlab.com:9005/usuarioApp/login';
     const json = JSON.stringify(this.logeo)
     const httpOptions = {
       headers: new HttpHeaders({
         'Content-Type': 'application/json'
       })
     };
     return this.http.post(dir, json, httpOptions);
   }
   
   olvidaste_pass(){
     this.vent2 = false;
     this.vent3 = true;
   }
   cancel(){
     this.vent0 = true;
     this.vent3 = false;
   }
   registro(){
     this.vent2 = false;
     this.vent1 = true;
   }
   login_registro(){
     this.vent1 = false;
     this.vent2 =true;
   }
   newEmailToken(){
     this.responseEmailToken().subscribe(solicitud => {
       if(solicitud.status === 404){
         this.bandalerta = true;
         setTimeout(() => {
           this.bandalerta = false;
           this.sms = '¡El usuario no existe!';
         }, 1000);
       }
       this.loader = false;
       this.vent3 = false;
       this.vent4 =true;
       this.pswd = null;
       console.log("R: ",solicitud);
     })
   }
   responseEmailToken(): Observable<any>{
     this.loader = true;
     const dir = 'https://hyperiontechlab.com:9005/usuarioApp/sendEmailToken';
     const json = JSON.stringify(this.restpass)
     const httpOptions = {
       headers: new HttpHeaders({
         'Content-Type': 'application/json'
       })
     };
     return this.http.post(dir, json, httpOptions);
   }
 
   responsePasswordChange(): Observable<any>{
     let request = {
       token: this.tokennew,
       newpass: this.passwordnew
     }
     const dir = 'https://hyperiontechlab.com:9005/usuarioApp/changePassUserPass';
     const json = JSON.stringify(request)
     const httpOptions = {
       headers: new HttpHeaders({
         'Content-Type': 'application/json'
       })
     };
     return this.http.post(dir, json, httpOptions);
   }
   passwordChange(){
     this.responsePasswordChange().subscribe(solicitud => {
       console.log("S: ",solicitud);
     })
   }
 
   pswd: any;
   tokenmail: any;
   tokennew: any;
   restpass = {
     email: ''
   }
   restablecer_pass(){
     console.log("EM: ",this.restpass.email);
     if(this.restpass.email === undefined || this.restpass.email === '' || this.restpass.email === null){
       this.bandalerta = true;
       setTimeout(() => {
         this.sms = '¡Campos Incompletos!';
         this.bandalerta = false;
       }, 1000);
     }else{
      this.pswd = this.restpass.email;
      console.log("PSW: ",this.pswd);
      this.restpass.email = null;
       this.newEmailToken();
     }
   }
   cancel_pass(){
     this.vent4 = false;
     this.vent0 = true;
   }
   pass(){
     if(this.tokenmail === undefined || this.tokenmail === '' || this.tokenmail === null){
       this.bandalerta = true;
       setTimeout(() => {
         this.bandalerta = false;
         this.sms =  '¡Campos Incompletos!';
       }, 1000);
     }else{
       this.tokennew = this.tokenmail;
       this.vent4 = false;
       this.vent5 = true;
     }
   }
   passwordnew;
   confirmpassword;
   cancel_contras(){
     this.vent5 = false;
     this.vent0 = true;
   }
   succes(){
     if(this.passwordnew === undefined ||this.passwordnew === '' || this.passwordnew === null ||
     this.confirmpassword === undefined || this.confirmpassword === '' || this.confirmpassword === null){
      this.bandalerta = true;
       setTimeout(() => {
         this.bandalerta = false;
         this.sms =  '¡Campos Incompletos!';
       }, 1000);
     }else{
       if(this.passwordnew === this.confirmpassword){
        this.vent5 = false;
        this.vent6 = true;
        this.passwordChange();
       }else{
        this.band_pass_iguales = true;
        setTimeout(() => {
          this.band_pass_iguales = false;
          this.sms_pass_ig =  '¡Las contraseñas deben ser iguales!';
        }, 1000);
       }
       
     }
   }
   change_success(){
     this.vent6 = false;
     this.vent0 = true;
   }
   showPass(){
     if(this.typepass == 'password'){
       this.typepass = 'text'
     }else{
       this.typepass = 'password';
     }
     console.log('text', this.typepass);
   }
   register_facebook(){
     console.log("Registro Facebook");
   }
   contras_user = true;
   contras_user2 = true;
   user: any = {};
   name_user;
   correo_user;

   register_google(){
    this.googlePlus.login({})
    .then(res => {
      alert("GooglePlus: "+ JSON.stringify(res));
      this.user = res;
      this.name_user = res.displayName;
      this.correo_user = res.email;
      console.log("Datos: "+"Nombre: "+res.displayName+"Correo: "+res.email);
      this.registerUser.name = this.name_user;
      this.registerUser.email = this.correo_user;
      this.registerUser.password = ' ';
      this.passworddos = ' ';
      this.contras_user = false;
      this.contras_user2 = false;
      //this.getData();
      console.log("Respuesta: ",res);
      this.creo_user = 3;
      localStorage.setItem('welcome_user',this.creo_user);
    })
    .catch(err => console.error(err));
   }
   creo_user;
   getData(){
    this.http.get('https://www.googleapis.com/plus/v1/people/me?access_token='+this.user.access_token)
    .subscribe((data:any)=>{
      console.log("Datos Obtenidos: ",data);
    })
  }
   acces_facebook(){
     console.log("Acceso Facebook");
   }
   login_google_pass;
   acces_google(){
    this.googlePlus.login({})
    .then(res => {
      const w_us = localStorage.getItem('welcome_user');
      this.login_google_pass = res.email;
      this.LoginToGoogle();
      console.log("Datos: "+"Nombre: "+res.displayName+"Correo: "+res.email);
    })
    .catch(err => console.error(err));
   }

   LoginToGoogle(){
     this.responseLogintoGoogle().subscribe(data=>{
       if(data.status === 200){
         setTimeout(() => {
          this.loader = false
          this.texto = 'Cargando...';
          this.route.navigate(['/home']);
         }, 1000);
       }
       console.log("Middle Login: ",data);
     })
   }

   responseLogintoGoogle(): Observable<any>{
     this.loader = true;
     var request = {
      email: this.login_google_pass
     }
     const dir = 'https://hyperiontechlab.com:9005/usuarioApp/middleLogin';
     const json = JSON.stringify(request)
     const httpOptions = {
       headers: new HttpHeaders({
         'Content-Type': 'application/json'
       })
     };
     return this.http.post(dir, json, httpOptions);
   }
}
