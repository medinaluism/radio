import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-programacion-tv',
  templateUrl: './programacion-tv.page.html',
  styleUrls: ['./programacion-tv.page.scss'],
})
export class ProgramacionTvPage implements OnInit {
  @Input() texto = 'Cargando...';
  loader = false;
  constructor(private route: Router,
    private http: HttpClient, private menu: MenuController) { }

  ngOnInit() {
    this.menu.enable(true);
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
    }, 7500);
  }
  getAllTv(){
    this.menu.enable(true);
    const dir = ' https://hyperiontechlab.com:9016/livetvapp/getStreamingTvApp';
    return this.http.get(dir)
    .subscribe((solicitud)=>{
      console.log("tvs: ",solicitud);
    })
  }
  home(){
    this.route.navigate(['/home']);
    this.menu.enable(true);
  }
}
