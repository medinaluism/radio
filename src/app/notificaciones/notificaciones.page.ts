import { Component, OnInit, SecurityContext } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { MenuController, NavController } from '@ionic/angular';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { HomePage } from '../home/home.page';

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.page.html',
  styleUrls: ['./notificaciones.page.scss'],
})
export class NotificacionesPage implements OnInit {

  //ruta_notificacion:SafeResourceUrl;
  constructor(private route: Router, private http: HttpClient,
    private navCtrl: NavController, private menu: MenuController, private dom: DomSanitizer) { 
      //console.log("Que traigo en Storage: ", localStorage.getItem('link_not'));
      //this.ruta_notificacion = this.dom.bypassSecurityTrustResourceUrl(localStorage.getItem('link_not'));

      //console.log("Informacion: ",this.ruta_notificacion);
      //console.log(this.ruta_notificacion.changingThisBreaksApplicationSecurity);


    }
    ruta_notificacion: any;
    valor_ruta;
  ngOnInit() {
    this.menu.close();
    this.menu.enable(true);
    this.valor_ruta = localStorage.getItem('link_not');
    this.ruta_notificacion = this.valor_ruta;
    //this.ruta_notificacion = this.dom.bypassSecurityTrustResourceUrl(this.valor_ruta);

    console.log("Local Storage: ",this.valor_ruta);
  }

  home(){
    this.menu.enable(true);
    this.route.navigate(['/home']);
  }

}
