import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { MenuController } from '@ionic/angular';
@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.page.html',
  styleUrls: ['./noticias.page.scss'],
})
export class NoticiasPage implements OnInit {
  @Input() texto = 'Cargando...';
  loader = false;
  bandalerta = false;
  noticias;
  new;
  by_id = {
    id: 1
  }
  noticias_all = true;
  page_noticia = false;
  constructor(private route: Router, 
    private http: HttpClient, private dom: DomSanitizer, private menu: MenuController) { }

  ngOnInit() {
    this.menu.enable(true);
    this.getAllNews();
      const tok = localStorage.getItem('token_act');
      console.log("Noticia_ Token: ",tok);
  }
  
  getAllNews(){
    this.loader = true;
    this.menu.enable(true);
    const dir = 'https://hyperiontechlab.com:9015/news/getAllNews';
    return this.http.get(dir)
    .subscribe((solicitud)=>{
        this.loader = false;
        this.texto = 'Cargando...';
        this.page_noticia = false;
        this.noticias_all = true;
        this.new = solicitud;
        this.noticias = this.new.data;
        console.log("Noticias: ",solicitud);
    }, (err)=>{
      if(err.error.status = 0){
        console.log("Error al cargar datos");
      }
    })
  }
  
  ruta_noticia;
  Object(val){
    this.menu.enable(true);
    this.loader = true;
    this.page_noticia = true;
    this.noticias_all = false;
    this.ruta_noticia = this.dom.bypassSecurityTrustResourceUrl(val.url);
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
    }, 4500);
  }
  home(){
    this.route.navigate(['/home']);
    this.menu.enable(true);
  }
}
