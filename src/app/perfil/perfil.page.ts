import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
  @Input() texto = 'Cargando...';
  @Input() sms = '¡Campos Incompletos!';
  @Input() sms_pass = '¡Las contraseñas deben ser iguales!';
  bandalerta = false;
  bandalerta_pass = false;
  loader = false;
  edt_p = "<";
  desv_cuentas = false;
  vinc_cuentas = false;
  date_user = {
    userAppId : null,
    name: '',
    email: '',
    password: '',
    location: '',
    pushActive: null
  }
  password_uno;
  password_dos;
  editname;
  editperfil;
  editcontrasenia;
  verdad;
  activo = 1;
  inactivo = 0;
  constructor(private route: Router, private http: HttpClient, 
    private menu: MenuController, private nav: NavController) { }
  
  ngOnInit() {
    this.editperfil = true;
    this.vinc_cuentas = true;
    this.desv_cuentas = false;
    this.menu.enable(false);
    this.Logeo();
  }
  
  edit_nombre(){
    this.edt_p = "<";
    this.editname = true;
    this.editperfil = false;
    this.menu.enable(false);
    this.Logeo();
  }
  save_name(){
    if(this.date_user.name === null || this.date_user.name === '' || this.date_user.name === undefined){
      this.bandalerta = true;
      setTimeout(() => {
        this.bandalerta = false;
         this.sms = '¡Campos Incompletos!';
      }, 1000);
    }else{
    this.responseEditUser().subscribe(solicitud=>{
      this.loader = false;
      this.editperfil = true;
      this.editname = false;
      console.log("R: ",solicitud);
    })
  }
  }
  responseEditUser(): Observable<any>{
    this.loader = true;
     const dir = 'https://hyperiontechlab.com:9005/usuarioApp/updateUser';
     const json = JSON.stringify(this.date_user);
     console.log("Que mando update: ",json);
     const httpOptions = {
       headers: new HttpHeaders({
         'Content-Type': 'application/json'
       })
     };
     return this.http.post(dir, json, httpOptions);

  }

  edit_password(){
    this.edt_p = "<";
    this.editname = false;
    this.editperfil = false;
    this.editcontrasenia = true;
    this.menu.enable(false);
    this.Logeo();
  }

  cambio_contrasenia;
  save_password(){
    if(this.date_user.password === null || this.date_user.password === '' || this.date_user.password === undefined
    || this.password_uno === null || this.password_uno === '' || this.password_uno === undefined 
    || this.password_dos === null || this.password_dos === '' || this.password_dos === undefined){
      this.bandalerta = true;
      setTimeout(() => {
        this.bandalerta = false;
         this.sms = '¡Campos Incompletos!';
      }, 1000);
    }else{
      if(this.password_uno === this.password_dos){
        this.cambio_contrasenia = 1;
        localStorage.setItem('change_psswd',this.cambio_contrasenia);
        this.responseEditUserPass().subscribe(solicitud=>{
          console.log("-->Pas: ",solicitud);
          this.loader = false;
          this.route.navigate(['/register']);
          this.dato = 2;
          localStorage.setItem('active',this.dato);
          //this.nav.navigateForward('/register');
          //this.Logeo();
          //this.editperfil = true;
          //this.editcontrasenia = false;
        })
      }else{
        this.bandalerta_pass = true;
        setTimeout(() => {
          this.bandalerta_pass = false;
          this.sms_pass = '¡Las contraseñas deben ser iguales!';
        }, 1000);
      }
    }
  }


  responseEditUserPass(): Observable<any>{
    this.loader = true;
    var request = {
      userAppId: this.date_user.userAppId,
      name: this.date_user.name,
      email: this.date_user.email,
      password: this.password_uno
    }
    const dir = 'https://hyperiontechlab.com:9005/usuarioApp/updateUser';
    const json = JSON.stringify(request)
    console.log("Que mando update: ",json);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(dir, json, httpOptions);
  }
  Logeo(){
    this.responseLogeo().subscribe(solicitud=>{
      this.loader = false;
      this.date_user = solicitud.data;
      console.log("Sol: ",solicitud);
    })
  }
  responseLogeo(): Observable<any>{
    this.loader = true;
      var request = {
        email: localStorage.getItem('ema_il'),
        password: localStorage.getItem('pass_word')
      }
       const dir = 'https://hyperiontechlab.com:9005/usuarioApp/login';
       const json = JSON.stringify(request)
       const httpOptions = {
         headers: new HttpHeaders({
           'Content-Type': 'application/json'
         })
       };
       return this.http.post(dir, json, httpOptions);
  }

  activar_notificacion(){
    this.verdad = this.date_user.pushActive;
    if (this.verdad === true) {
      this.verdad = this.activo;
      this.responseEditStatus().subscribe(data=>{
      })
    }
    if (this.verdad === false) {
      this.verdad = this.inactivo;
      this.responseEditStatus().subscribe(data=>{
      })
    }
  }

  responseEditStatus(): Observable<any>{
    if(this.verdad === 1){
      var request = {
        userAppId: this.date_user.userAppId,
        pushActive : true
      }
      const dir = 'https://hyperiontechlab.com:9005/usuarioApp/updateUser';
      const json = JSON.stringify(request)
      console.log("Que mando update: ",json);
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
      return this.http.post(dir, json, httpOptions);
    }else{
      if(this.verdad === 0){
        var request = {
          userAppId: this.date_user.userAppId,
          pushActive : false
        }
        const dir = 'https://hyperiontechlab.com:9005/usuarioApp/updateUser';
        const json = JSON.stringify(request)
        console.log("Que mando update: ",json);
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json'
          })
        };
        return this.http.post(dir, json, httpOptions);
      }
    }
  }

  edit_count_user(){
    this.route.navigate(['/home']);
    this.menu.enable(true);
  }
  exit_name(){
    this.editname = false;
    this.editperfil = true;
    this.menu.enable(false);
  }
  exit_password(){
    this.editcontrasenia = false;
    this.editperfil = true;
    this.menu.enable(false);
  }
  dato;
  close_application(){
    /*this.dato = 2;
    localStorage.setItem('active',this.dato);
    this.route.navigate(['/register']);
    this.menu.enable(false);
    this.closeSessionApp();
    this.desactivePush();*/

    this.responseCloseSession().subscribe(solicitud=>{
      setTimeout(() => {
        this.route.navigate(['/register']);
        this.menu.enable(false);
        this.loader = false;
        this.sms = this.texto;
        this.dato = 2;
        localStorage.setItem('active',this.dato);
        //this.closeSessionApp();
        this.desactivePush();
      }, 3000);
      console.log("Salí de Perfil: ",JSON.stringify(solicitud));
    })

  }

  /*closeSessionApp(){
    this.responseCloseSession().subscribe(solicitud=>{
      setTimeout(() => {
        this.sms = this.texto;
        this.loader = false;
      }, 3000);
      console.log("Salí de App: ",solicitud);
    })
  }*/
  responseCloseSession(): Observable<any>{
    this.loader = true;
    var request = {
      email: this.date_user.email
    }
    const dir = 'https://hyperiontechlab.com:9005/usuarioApp/closeSession';
    const json = JSON.stringify(request)
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(dir, json, httpOptions);
  }



  desactivePush(){
      this.responsedesactivePush().subscribe(solicitud=>{
        console.log("Cerré sesion : ",solicitud);
      })
  }
  responsedesactivePush(): Observable<any>{
      var request = {
        userAppId: this.date_user.userAppId,
        pushActive: false
      }
      const dir = 'https://hyperiontechlab.com:9005/usuarioApp/updateUser';
      const json = JSON.stringify(request)
      console.log("Mando en responseClosesesion: ",request);
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
      return this.http.post(dir, json, httpOptions);
  }

  des_cuenta(){
    this.desv_cuentas = true;
    this.vinc_cuentas = false;
  }
  vinc_cuenta(){
    this.desv_cuentas = false;
    this.vinc_cuentas = true;
  }
}
