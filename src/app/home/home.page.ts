import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { MenuController, NavController, ToastController, PopoverController, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { FCM, NotificationData } from '@ionic-native/fcm/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  @Input() texto = 'Cargando...';
  @Input() noti_valor;
  loader = false;
  imagen_slid;
  img_uno;
  img_dos;
  img_tres;

  slideOpt = {
    loop: true,
    autoplay:true
  };
  constructor(private route: Router, private dom: DomSanitizer, public menu: MenuController, private http: HttpClient, 
    private navCtrl: NavController, private fcm: FCM, public toastCtrl: ToastController, public popoverCtrl: PopoverController, public loadingCtrl: LoadingController) {}

    ngOnInit(){
      this.menu.enable(true);
      this.getHome();
    }
 
    getHome(){
      this.menu.enable(true);
      //this.loader = true;
      const dir = 'https://hyperiontechlab.com:9017/home/getHome';
      return this.http.get(dir)
      .subscribe(solicitud=>{
        console.log("Sol: ",solicitud);

        if(solicitud != null){
          console.log("Error");
        }
        this.texto = 'Cargando...'
        this.imagen_slid = solicitud;
        this.img_uno = this.imagen_slid.data[0].pictureOne;
        this.img_dos = this.imagen_slid.data[0].pictureTwo;
        this.img_tres = this.imagen_slid.data[0].pictureTree;
        console.log("->: ",solicitud);

      },(err)=>{
        if(err.error.status === 0){
          console.log("Error al cargar datos");
        }
      })
    }


    Tv_vivo(){
      this.route.navigate(['/tv-vivo']);
      this.menu.enable(false);
    }
    Programacion_tv(){
      this.route.navigate(['/programacion-tv']);
      this.menu.enable(true);
    }
    Radio(){
      this.route.navigate(['/radio']);
      this.menu.enable(true);
    }
    Noticias(){
      this.route.navigate(['/noticias']);
      this.menu.enable(true);
    }

}
