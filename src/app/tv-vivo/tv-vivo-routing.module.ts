import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TvVivoPage } from './tv-vivo.page';

const routes: Routes = [
  {
    path: '',
    component: TvVivoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TvVivoPageRoutingModule {}
