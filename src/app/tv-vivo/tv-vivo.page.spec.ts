import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TvVivoPage } from './tv-vivo.page';

describe('TvVivoPage', () => {
  let component: TvVivoPage;
  let fixture: ComponentFixture<TvVivoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TvVivoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TvVivoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
